<?php

namespace Drupal\Tests\jsonapi_permission_access\JsonApiPermissionAccessTest;

use Drupal\Tests\BrowserTestBase;

/**
 * Testing JSON:API Permission Access.
 *
 * @group jsonapi_permission_access
 */
class JsonApiPermissionAccessTest extends BrowserTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'node',
    'jsonapi',
    'serialization',
    'jsonapi_permission_access',
  ];

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * Setup function.
   */
  protected function setUp(): void {
    parent::setUp();
    // Create an article content type for testing.
    $this->drupalCreateContentType([
      'type' => 'article',
      'name' => 'Article',
    ]);

    // Create article which will have NID.
    $this->drupalCreateNode([
      'type' => 'article',
      'title' => 'Article for testing.',
    ]);

    $this->container->get('router.builder')->rebuild();
  }

  /**
   * Tests that a 403 is returned on JSON:API routes.
   */
  public function testJsonApiStatusCode() {
    $account = $this->drupalCreateUser(['access content']);
    $this->drupalLogin($account);
    $this->drupalGet('jsonapi/node/article');
    $this->assertSession()->statusCodeEquals(403);
  }

  /**
   * Tests that a user with the correct permission can access.
   */
  public function testUserAccessWithPermission() {
    $account = $this->drupalCreateUser(['access jsonapi routes']);
    $this->drupalLogin($account);
    $this->drupalGet('jsonapi/node/article');
    $this->assertSession()->statusCodeEquals(200);
  }

}
