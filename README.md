## JSON:API Permission Access

### Installation & setup
Download and install the module. [Here is a helpful guide](https://www.drupal.org/docs/extending-drupal/installing-modules#s-step-1-add-the-module)

Once enabled, you can navigate to `/admin/people/permissions`and
assign the new permission to any role to grant access.

#### JSON:API Role
There is a role created by default called `JSON:API User`
which has only this permission attached to it.
